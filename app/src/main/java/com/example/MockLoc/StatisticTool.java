package com.example.MockLoc;

import android.content.Context;
import android.location.Location;

import com.tencent.stat.MtaSDkException;
import com.tencent.stat.StatConfig;
import com.tencent.stat.StatReportStrategy;
import com.tencent.stat.StatService;

import java.util.Properties;

/**
 * Author: Gordon
 * Date: 2014/7/7 14:16
 * MTA tool
 */
public class StatisticTool {

    private final static String EVENT_ID_位置记录 = "on_location_change";

    public static void init(Context context) {
        StatConfig.setDebugEnable(false);
        StatConfig.setStatSendStrategy(StatReportStrategy.PERIOD);
        StatConfig.setSendPeriodMinutes(1);
        try {
            StatService.startStatService(context, "A2YP5TQ6N2RV", com.tencent.stat.common.StatConstants.VERSION);
        } catch (MtaSDkException e) {
            e.printStackTrace();
        }
    }

    public static void trackLocationUsed(Context context, Location location) {
        Properties properties = new Properties();
        properties.setProperty("location", location.getLongitude() + ";" + location.getLatitude());
        StatService.trackCustomKVEvent(context, EVENT_ID_位置记录, properties);
    }

}
