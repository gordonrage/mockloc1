package com.example.MockLoc.map;

/**
 * Created by Gordon on 15/3/28.
 * TODO:
 */
public class MapEvents {

    public static final class OnMapClickEvent{
        String longitude;
        String latitude;

        public OnMapClickEvent(String longitude, String latitude) {
            this.longitude = longitude;
            this.latitude = latitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public String getLongitude() {
            return longitude;
        }
    }
}
