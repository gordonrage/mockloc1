package com.example.MockLoc.map;

import android.webkit.JavascriptInterface;
import com.example.MockLoc.GD;
import com.example.MockLoc.MainActivity;

/**
 * Author: Gordon
 * Date: 2014/6/20 15:02
 * Todo:
 */
public class WebMapJsInterface {

    MainActivity activity;

    public WebMapJsInterface(MainActivity activity) {
        this.activity = activity;
    }

    /**
     * get location geo point from web view.
     */
    @JavascriptInterface
    public void getLocation(final String locationString) {
        GD.i(locationString);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.parseGeoString(locationString);
            }
        });
    }

}
