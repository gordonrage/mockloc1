package com.example.MockLoc.map.baidu;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.example.MockLoc.R;
import com.example.MockLoc.map.MapEvents;

import de.greenrobot.event.EventBus;

/**
 * Created by Gordon on 15/3/28.
 * TODO:
 */
public class BaiduMapFragment extends Fragment {

    private final String TAG = getClass().getSimpleName();

    BaiduMap baiduMap;

    MapView mMapView;

    Bitmap bmp_Location ;

    int dp16;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return new MapView(inflater.getContext());
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMapView = (MapView) view;
        baiduMap = mMapView.getMap();
        dp16 = getResources().getDimensionPixelSize(R.dimen.dp16);
        bmp_Location = BitmapFactory.decodeResource(getResources(), R.drawable.ic_location_1);
        bmp_Location = Bitmap.createScaledBitmap(bmp_Location, dp16, dp16, false);
        baiduMap.setOnMapClickListener(new BaiduMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                showClickWindow(latLng);
            }

            @Override
            public boolean onMapPoiClick(MapPoi mapPoi) {
                showClickWindow(mapPoi.getPosition());
                return false;
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        mMapView.onDestroy();
    }
    @Override
    public void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        mMapView.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
        mMapView.onPause();
    }

//    @Override
//    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
//
//        Animator animator;
//        if (enter) {
//            animator = ObjectAnimator.ofFloat(this, "translationY", getResources().getDisplayMetrics().heightPixels, 0);
//        } else {
//            animator = ObjectAnimator.ofFloat(this, "translationY", 0, getResources().getDisplayMetrics().heightPixels);
//        }
//        animator.setDuration(300);
//        return animator;
//    }


    private void showClickWindow(LatLng latLng){
        Log.i(TAG, latLng.toString());
        EventBus.getDefault().post(new MapEvents.OnMapClickEvent(String.valueOf(latLng.longitude),
                String.valueOf(latLng.latitude)));
        //定义Maker坐标点
//构建Marker图标


        BitmapDescriptor bitmap = BitmapDescriptorFactory.fromBitmap(bmp_Location);
//构建MarkerOption，用于在地图上添加Marker
        OverlayOptions option = new MarkerOptions()
                .position(latLng)
                .icon(bitmap);
//在地图上添加Marker，并显示
        baiduMap.clear();
        baiduMap.addOverlay(option);
    }


}
