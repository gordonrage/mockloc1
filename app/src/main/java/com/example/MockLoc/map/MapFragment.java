package com.example.MockLoc.map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import com.example.MockLoc.CurrentApp;
import com.example.MockLoc.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Author: Gordon
 * Date: 2014/6/20 10:22
 * Todo:
 */
public class MapFragment extends Fragment {


    CurrentApp app;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        app = CurrentApp.obtainApp(activity);
    }

    private ImageView imgv_CentralLocation;
    private WebView webView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.act_map, container, false);
        imgv_CentralLocation = (ImageView) root.findViewById(R.id.act_map__ImageView_CentralLocation);
        webView = (WebView) root.findViewById(R.id.act_map__WebView_Map);
        return root;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (!url.contains("ditu.google.cn")) {
                    return;
                }
                view.addJavascriptInterface(new WebMapJsInterface((com.example.MockLoc.MainActivity) getActivity()), "fuck_off__");
                try {
                    injectJQuery();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        WebSettings webSettings = webView.getSettings();
//        webSettings.setUserAgentString("Chrome 37 beta");
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        if (Build.VERSION.SDK_INT >= 16) {
            webSettings.setAllowFileAccessFromFileURLs(true);
            webSettings.setAllowUniversalAccessFromFileURLs(true);
        }
        webView.loadUrl("http://ditu.google.cn/");
        imgv_CentralLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMapContextMenu();
            }
        });

        //  hack listener in activity
        getActivity().findViewById(R.id.act_main__Button_GetLocation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                webView.loadUrl("javascript:fuck_off__.getLocation($('#q_d-pos2').size())");
                webView.loadUrl("javascript:fuck_off__.getLocation(document.getElementById(\"q_d-pos2\").children[0].value)");
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        webView.destroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void injectJQuery() throws IOException {
        InputStream in = getActivity().getAssets().open("jquery.js");
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = in.read(buffer)) != -1) {
            bao.write(buffer, 0, len);
        }
        in.close();
        String jqueryCode = new String(bao.toByteArray(), "utf8");
        webView.loadUrl("javascript:" + jqueryCode);
    }

    private final static String jsCode = "javascript:" +
            "!function(){var a=document.createEvent(\"MouseEvents\");a.initMouseEvent(\"contextmenu\",!0,!0,window,1,window.innerWidth/2,window.innerHeight/2,window.innerWidth/2,window.innerHeight/2,!1,!1,!0,!1,0,null),document.getElementById(\"viewContainer\").dispatchEvent(a),setTimeout(function(){$(\".kd-menulist\").children().last().click()},500)}();";


    private void showMapContextMenu() {
        webView.loadUrl(jsCode);
    }
}
