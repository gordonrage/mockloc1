package com.example.MockLoc;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Author: Gordon
 * Date: 2014/6/19 12:55
 * Todo:
 */
public class GMapActivity extends Activity implements View.OnClickListener {


    private WebView webView;

    private ImageView imgv_CentralLocation;
    CurrentApp app;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_map);
        app = CurrentApp.obtainApp(this);
        app.addActivity(this);
        webView = (WebView) findViewById(R.id.act_map__WebView_Map);
        imgv_CentralLocation = (ImageView) findViewById(R.id.act_map__ImageView_CentralLocation);

        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                try {
                    injectJQuery();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        WebSettings webSettings = webView.getSettings();
//        webSettings.setUserAgentString("Chrome 37 beta");
        webSettings.setDisplayZoomControls(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        if (Build.VERSION.SDK_INT >= 16) {
            webSettings.setAllowFileAccessFromFileURLs(true);
            webSettings.setAllowUniversalAccessFromFileURLs(true);
        }
        webView.loadUrl("http://ditu.google.cn/");
        imgv_CentralLocation.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
//        GD.toast(v, getApplicationContext());
        showMapContextMenu();
    }

    @Override
    protected void onDestroy() {
        app.removeActivity(this);
        super.onDestroy();
    }

    private void injectJQuery() throws IOException {
        InputStream in = getAssets().open("jquery.js");
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = in.read(buffer)) != -1) {
            bao.write(buffer, 0, len);
        }
        in.close();
        String jqueryCode = new String(bao.toByteArray(), "utf8");
        webView.loadUrl("javascript:" + jqueryCode);
        // test inject
//        webView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                webView.loadUrl("javascript:" + "$(function(){alert(\"Document ready\")});\n");
//            }
//        }, 1000);
    }

    private final static String jsCode = "javascript:" +
        "!function(){var a=document.createEvent(\"MouseEvents\");a.initMouseEvent(\"contextmenu\",!0,!0,window,1,window.innerWidth/2,window.innerHeight/2,window.innerWidth/2,window.innerHeight/2,!1,!1,!0,!1,0,null),document.getElementById(\"viewContainer\").dispatchEvent(a),setTimeout(function(){$(\".kd-menulist\").children().last().click()},500)}();";


    private void showMapContextMenu() {
        webView.loadUrl(jsCode);
    }
}
