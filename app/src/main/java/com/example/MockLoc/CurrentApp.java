package com.example.MockLoc;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Process;
import android.preference.PreferenceManager;

import com.baidu.mapapi.SDKInitializer;
import com.example.MockLoc.history.DbHelper;
import com.example.MockLoc.setting.SettingsActivity;

import java.util.LinkedList;

/**
 * @author daiyu
 */
public class CurrentApp extends Application {


    public double longitude;
    public double latitude;
//    public long locatingPeriod;
//    public boolean displayNotification;

    public int currentThemeId = R.style.AppThemeLight;

    public DbHelper dbHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        reloadCurrentTheme();

        dbHelper = new DbHelper(this);

        StatisticTool.init(this);

        SDKInitializer.initialize(getApplicationContext());
    }


    /**
     * 当前应用运行的Activity， 主要是为了让应用能得到当前用户界面的Activity。
     * 而且能退出应用时全部销毁。
     */
    private LinkedList<Activity> activities = new LinkedList<Activity>();

    /**
     * 当Activity 被create的时候放进来
     *
     * @param activity 被创建的Activity
     */
    public void addActivity(Activity activity) {
        activities.add(activity);
        GD.i("当前栈数量:" + activities.size());
        echoAllActivity();
    }

    /**
     * 当某个Activity destroy的时候
     *
     * @param activity 被销毁的Activity
     */
    public void removeActivity(Activity activity) {
        activities.remove(activity);
        GD.i("当前栈数量:" + activities.size());
        echoAllActivity();
    }


    /**
     * Test an activity is  whether in the activities stack.
     *
     * @param cls Activity class
     * @return true if contains this activity
     */
    public boolean testActivityInStack(Class cls) {
        for (Activity activity : activities) {
            if (activity != null && activity.getClass() == cls) {
                return true;
            }
        }
        return false;
    }

    public Activity getActivity(Class clz) {
        for (Activity activity : activities) {
            if (activity != null && activity.getClass() == clz) {
                return activity;
            }
        }
        return null;
    }


    private void reloadCurrentTheme() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String[] arr_values = getResources().getStringArray(R.array.arr_themes_values);
        final String theme_light = arr_values[0];
        final String choseTheme = preferences.getString(SettingsActivity.KEY_THEME, theme_light);
        if (choseTheme.equals(theme_light)) {
            currentThemeId = R.style.AppThemeLight;
        } else {
            currentThemeId = R.style.AppThemeDark;
        }
    }

    /**
     * @deprecated 发布时一定要关闭
     */
    @Deprecated
    private void echoAllActivity() {
        for (Activity activity : activities) {
            GD.i("当前栈：" + activity.getClass());
        }
    }


    /**
     * 退出应用
     */
    public void exit() {
        for (Activity activity : activities) {
            if (activity != null) {
                activity.finish();
            }
        }
        Process.killProcess(Process.myPid());
    }

    /**
     * 获取到Application类
     * http://stackoverflow.com/questions/3826905/singletons-vs-application-context-in-android
     *
     * @param context 本应用的一个context
     * @return CurrentApp对象
     */
    public static CurrentApp obtainApp(Context context) {
        return (CurrentApp) context.getApplicationContext();
    }

}