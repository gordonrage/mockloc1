package com.example.MockLoc.setting;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.NumberPicker;
import com.example.MockLoc.R;

/**
 * Author: Gordon
 * Date: 2014/6/17 11:02
 * Todo:
 */
public class NumberPickDialog extends Dialog {

    OnNumberPickOverListener onNumberPickOverListener;


    public NumberPickDialog(Context context, OnNumberPickOverListener listener) {
        super(context);
        initView(context);
        this.onNumberPickOverListener = listener;
    }

    NumberPicker numberPicker;

    private void initView(Context context) {
        setTitle(R.string.s_choose_time);
        setContentView(R.layout.dialog_number_picker);
        numberPicker = (NumberPicker) findViewById(R.id.dialog_number_picker__NumberPicker);
        numberPicker.setMaxValue(60);
        numberPicker.setMinValue(1);
        findViewById(R.id.dialog_number_picker__Button_OK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNumberPickOverListener.onGetNumber(numberPicker.getValue());
                dismiss();
            }
        });
        findViewById(R.id.dialog_number_picker__Button_Cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void setDefaultValue(int value) {
        numberPicker.setValue(value);
    }


    public interface OnNumberPickOverListener {
        public void onGetNumber(int number);
    }

}
