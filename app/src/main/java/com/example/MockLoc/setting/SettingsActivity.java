package com.example.MockLoc.setting;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import com.example.MockLoc.CurrentApp;
import com.example.MockLoc.R;
import com.example.MockLoc.UiUtil;

/**
 * Author: Gordon
 * Date: 13-12-25 下午9:10
 * Application setting activity
 */
public class SettingsActivity extends PreferenceActivity {


    /**
     * Key of location update period
     */
    public final static String KEY_LOC_UPDATE_PERIOD = "key_location_update_period";
    public final static String KEY_DISPLAY_NOTIFICATION = "setting_allow_notification_view";
    public final static String KEY_DEBUG = "setting_debug";
    public final static String KEY_THEME = "setting_theme";

    public static final int DefaultPeriod = 10;

    SharedPreferences pref;

    private CurrentApp app;

    private Preference pref_UpdatePeriod, pref_Theme;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = CurrentApp.obtainApp(this);
        app.addActivity(this);
        setTheme(R.style.AppThemeDark);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        addPreferencesFromResource(R.xml.setting);

        pref_UpdatePeriod = findPreference(KEY_LOC_UPDATE_PERIOD);
        pref_Theme = findPreference(KEY_THEME);
        pref_UpdatePeriod.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(final Preference preference) {
                NumberPickDialog numberPickDialog = new NumberPickDialog(SettingsActivity.this, new NumberPickDialog.OnNumberPickOverListener() {
                    @Override
                    public void onGetNumber(int number) {
                        preference.getEditor().putInt(KEY_LOC_UPDATE_PERIOD, number).commit();
                        pref_UpdatePeriod.setSummary(getString(R.string.s_current_value) + number);

                    }
                });
                final int value = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this).getInt(KEY_LOC_UPDATE_PERIOD, DefaultPeriod);
                numberPickDialog.setDefaultValue(value);
                numberPickDialog.show();
                return false;
            }
        });


        findPreference(KEY_DISPLAY_NOTIFICATION).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Boolean bool = (Boolean) newValue;
                if (!bool) {
                    UiUtil.cancelAllNotifications(app);
                }
                return true;
            }
        });

        pref_Theme.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                showThemeSummary((String) newValue);
                reloadTheme((String) newValue);
                return true;
            }
        });

        //  ** display update period
        showUpdatePeriodSummary();

        //  ** display chose theme text
        final String currentChoseTheme = pref.getString(KEY_THEME, getString(R.string.setting_theme_light));
        showThemeSummary(currentChoseTheme);
    }

    private void showUpdatePeriodSummary() {
        final int period = pref.getInt(KEY_LOC_UPDATE_PERIOD, DefaultPeriod);
        pref_UpdatePeriod.setSummary(getString(R.string.s_current_value) + period);

    }

    private void showThemeSummary(String currentChoseTheme) {
        String[] arr_values = getResources().getStringArray(R.array.arr_themes_values);
        final String theme_light = arr_values[0];
        final String theme_darkness = arr_values[1];
        if (currentChoseTheme.equals(theme_darkness)) {
            pref_Theme.setSummary(R.string.setting_theme_darkness);
        } else {
            pref_Theme.setSummary(R.string.setting_theme_light);
        }
    }

    private void reloadTheme(String newTheme) {
        String[] arr_values = getResources().getStringArray(R.array.arr_themes_values);
        final String theme_light = arr_values[0];
        if (newTheme.equals(theme_light)) {
            app.currentThemeId = R.style.AppThemeLight;
        } else {
            app.currentThemeId = R.style.AppThemeDark;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        app.removeActivity(this);
    }
}
