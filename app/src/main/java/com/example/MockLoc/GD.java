package com.example.MockLoc;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class GD {

    private static final String TAG = "Tomato";

    public static void i(Object msg) {
        Log.i(TAG, String.valueOf(msg));
    }
    public static void i(String format, Object... o) {
        Log.i(TAG, String.format(format, o));
    }

    public static void w(Object msg) {
        Log.w(TAG, String.valueOf(msg));
    }

    public static void w(String format, Object... o) {
        Log.w(TAG, String.format(format, o));
    }
    public static void e(Object msg) {
        Log.e(TAG, String.valueOf(msg));
    }
    public static void e(String format, Object... o) {
        Log.e(TAG, String.format(format, o));
    }

    public static void d(Object msg) {
        Log.d(TAG, String.valueOf(msg));
    }
    public static void d(String format, Object... o) {
        Log.d(TAG, String.format(format, o));
    }

    public static void v(Object msg) {
        Log.v(TAG, String.valueOf(msg));
    }
    public static void v(String format, Object... o) {
        Log.v(TAG, String.format(format, o));
    }

    public static void error(Throwable tr) {
        e(Log.getStackTraceString(tr));
    }

    public static void toast(Object msg, Context context) {
        Toast.makeText(context, String.valueOf(msg), 0).show();
    }


    public static void stackHeap() {
        System.gc();
        GD.i("total memory: " + Runtime.getRuntime().totalMemory());
    }

    public static void dump() {
        //  todo dump file
    }


}