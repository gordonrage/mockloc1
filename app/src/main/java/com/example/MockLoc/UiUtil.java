package com.example.MockLoc;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.view.inputmethod.InputMethodManager;

import java.lang.reflect.Field;

/**
 * Author: Gordon
 * Date: 2014/5/14 11:10
 * Todo:
 */
public class UiUtil {

    /**
     * Hide IME in activity
     *
     * @param activity IME has shown in this Activity.
     */
    public static void hideInputMethod(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager)
                activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Get status bar height on any time.
     *
     * @param context ..
     * @return Status bar height
     */
    public static int getStatusBarHeight(Context context) {
        int result;
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId);
        } else {
            try {
                Class c = Class.forName("com.android.internal.R$dimen");
                Object obj = c.newInstance();
                Field field = c.getField("status_bar_height");
                int resId = Integer.parseInt(field.get(obj).toString());
                return resources.getDimensionPixelSize(resId);
            } catch (Exception e) {
                throw new RuntimeException("Can not retrieve the status bar height. your device is not supported.");
            }
        }
        return result;
    }

    /**
     * This method must call after  to Activity onCreated
     *
     * @return Status bar height
     */
    public static int getStatusBarHeightAccurately(Activity activity) {
        Rect rectangle = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);
        return rectangle.top;
    }

    public static void showNotificationView(Activity activity) {

    }

    public static void cancelAllNotifications(Context context) {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancelAll();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static String[] parseGeoFromClipboard(Context context) {
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboardManager.hasPrimaryClip()) {
            try {
                String s = (String) clipboardManager.getPrimaryClip().getItemAt(0).coerceToText(context);
                String[] parts = s.split(",");
                if (parts.length != 2) { //  format unavailable
                    return null;
                }
                //  just validate string
                Double.valueOf(parts[0]);
                Double.valueOf(parts[1]);
                return parts;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String[] parseGeoString(String geoString) {
        try {
            String[] parts = geoString.split(",");
            if (parts.length != 2) { //  format unavailable
                return null;
            }
            //  just validate string
            Double.valueOf(parts[0]);
            Double.valueOf(parts[1]);
            return parts;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
