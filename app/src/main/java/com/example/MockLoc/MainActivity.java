package com.example.MockLoc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.*;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.example.MockLoc.history.AddLocationHistoryDialog;
import com.example.MockLoc.history.DisplayHistoryActivity;
import com.example.MockLoc.map.MapEvents;
import com.example.MockLoc.map.baidu.BaiduMapFragment;
import com.example.MockLoc.setting.SettingsActivity;
import com.tencent.stat.StatConfig;
import com.tencent.stat.StatService;

import java.util.Properties;

import de.greenrobot.event.EventBus;

public class MainActivity extends Activity implements View.OnClickListener {


    CurrentApp app;


    private EditText et_Longitude, et_Latitude;
    private Button btn_StartMock;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = CurrentApp.obtainApp(this);
        app.addActivity(this);
        EventBus.getDefault().register(this);
        final String brand = Build.BRAND.toLowerCase();
        if (!brand.equals("xiaomi") && !brand.equals("google")){
            finish();
            Toast.makeText(this, R.string.msg_device_not_support, Toast.LENGTH_LONG).show();
            return;
        }
        setTheme(app.currentThemeId);
        setContentView(R.layout.act_main);
        et_Longitude = (EditText) findViewById(R.id.act_main__EditText_Longitude);
        et_Latitude = (EditText) findViewById(R.id.act_main__EditText_Latitude);
        btn_StartMock = (Button) findViewById(R.id.act_main__Button_StartMock);
        et_Latitude.setText("0");
        et_Longitude.setText("0");
        btn_StartMock.setOnClickListener(this);
        findViewById(R.id.act_main__Button_SaveLocation).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new AddLocationHistoryDialog(MainActivity.this, et_Latitude.getText().toString(),
                        et_Longitude.getText().toString()
                ).show();
            }
        });

        //  register clipboard listener
        ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        cm.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
            @Override
            public void onPrimaryClipChanged() {
                parseClipBoardGeoData();
            }
        });

        showDisclaimerDialog();
        detectMockDevSettingOpen();

        GD.i(Build.MANUFACTURER);
        GD.i(Build.BRAND);
        StatConfig.setAutoExceptionCaught(true);
        StatService.trackCustomEvent(this, "onCreate", "Main");
        Properties properties = new Properties();
        properties.setProperty("what", MainActivity.class.getCanonicalName());
        StatService.trackCustomKVEvent(this, "onCreate", properties);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        detectMockDevSettingOpen();
        et_Longitude.setText(String.valueOf(app.longitude));
        et_Latitude.setText(String.valueOf(app.latitude));
    }

    @Override
    public void onClick(View v) {
        if (v == btn_StartMock) {
            if (btn_StartMock.getText().equals(getString(R.string.s_start))) {
                final Double latitude = Double.valueOf(et_Latitude.getText().toString());
                final Double longitude = Double.valueOf(et_Longitude.getText().toString());
                app.latitude = latitude;
                app.longitude = longitude;
                startService(new Intent(this, LocationRetainService.class));
                btn_StartMock.setText(R.string.s_stop);
                if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsActivity.KEY_DISPLAY_NOTIFICATION, false)) {
                    showAsNotification();
                }
            } else {
                stopService(new Intent(this, LocationRetainService.class));
                btn_StartMock.setText(R.string.s_start);
                cancelNotification();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, R.id.menu_to_map, 1, R.string.s_map).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.add(0, R.id.menu_setting, 3, R.string.s_settings);
        menu.add(0, R.id.menu_history, 2, R.string.s_location_history);
        menu.add(0, R.id.menu_exit, 4, R.string.s_exit);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_exit:
                stopService(new Intent(this, LocationRetainService.class));
                android.os.Process.killProcess(Process.myPid());
                break;
            case R.id.menu_setting:
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                break;
            case R.id.menu_history:
                startActivity(new Intent(MainActivity.this, DisplayHistoryActivity.class));
                break;
            case R.id.menu_to_map:
//                startActivity(new Intent(this, GMapActivity.class));
                getFragmentManager().beginTransaction()
                        .replace(R.id.act_main__Frame_FragmentHolder, new BaiduMapFragment())
                        .commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Display the disclaimer statement
     */
    private void showDisclaimerDialog() {
        final String key = "disclaimer";
        final SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        boolean agreed = preferences.getBoolean(key, false);
        if (agreed) return;
        new AlertDialog.Builder(this).setTitle(R.string.s_disclaimer_title)
                .setMessage(R.string.s_disclaimer_summary)
                .setPositiveButton(R.string.s_agree, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit()
                                .putBoolean(key, true).commit();
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.s_disagree, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit()
                        .putBoolean(key, false).commit();
                dialog.cancel();
                finish();
            }
        }).setCancelable(false)
                .show();
    }


    @Override
    protected void onDestroy() {
        app.removeActivity(this);
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void showAsNotification() {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle(getString(R.string.s_running)).setSmallIcon(R.drawable.ic_launcher);
        builder.setAutoCancel(false).setOngoing(true);
        NotificationManager nm = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
//        Intent intent = new Intent(this, QuickSearchthis.class);
//        builder.setContentIntent(PendingIntent.getthis(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
        builder.setContentIntent(PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));

        //  custom view
        RemoteViews contentView = new RemoteViews(this.getPackageName(), R.layout.view_notification);
        //  click XX to close Notification.
        contentView.setOnClickPendingIntent(R.id.view_notification__ImageView_Close,
                PendingIntent.getBroadcast(this, 0, new Intent(NotificationBroadcast.INTENT_ACTION_CLOSE), PendingIntent.FLAG_ONE_SHOT));
        builder.setContent(contentView);
        nm.notify(1231328131, builder.build());

    }

    private void cancelNotification() {
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancelAll();
    }

    private void parseClipBoardGeoData() {
        String[] parts = UiUtil.parseGeoFromClipboard(MainActivity.this);
        if (parts != null) {
            et_Latitude.setText(parts[0]);
            et_Longitude.setText(parts[1]);
        }
    }

    public void parseGeoString(String geoString) {

        String[] parts = UiUtil.parseGeoString(geoString);
        if (parts != null) {
            et_Latitude.setText(parts[0]);
            et_Longitude.setText(parts[1]);
        }
    }

    private void detectMockDevSettingOpen() {
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        try {
            lm.addTestProvider(LocationManager.GPS_PROVIDER, false, false, false, false, true, false, false, 0, 5);
        } catch (SecurityException e) {
            if (e.getMessage().contains("Requires ACCESS_MOCK_LOCATION secure setting")) {
                Toast.makeText(this, getString(R.string.msg_without_mock_dev_setting), Toast.LENGTH_LONG).show();
                try {
                    startActivity(new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS));
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }

    }

    public void onEventMainThread(MapEvents.OnMapClickEvent event){
        et_Latitude.setText(event.getLatitude());
        et_Longitude.setText(event.getLongitude());
    }
}
