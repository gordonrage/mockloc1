package com.example.MockLoc.history;

import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.view.View;
import android.widget.EditText;
import com.example.MockLoc.R;

/**
 * Author: Gordon
 * Date: 2014/6/23 14:25
 * Todo:
 */
public class AddLocationHistoryDialog extends Dialog {

    public AddLocationHistoryDialog(final Context context, final String latitude, final String longitude) {
        super(context);

        setTitle(R.string.s_save_location);
        setContentView(R.layout.dialog_add_location);

        EditText et_Latitude = (EditText) findViewById(R.id.dialog_add_location__EditText_Latitude);
        EditText et_Longitude = (EditText) findViewById(R.id.dialog_add_location__EditText_Longitude);
        et_Latitude.setText(latitude);
        et_Longitude.setText(longitude);

        final EditText et_Remark = (EditText) findViewById(R.id.dialog_add_location__EditText_Remark);

        findViewById(R.id.dialog_add_location__Button_OK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String remark = et_Remark.getText().toString();
                Location location = new Location(LocationManager.GPS_PROVIDER);
                location.setLatitude(Double.valueOf(latitude));
                location.setLongitude(Double.valueOf(longitude));
                DAO_History.addLocationHistory(context, location, remark);
                dismiss();
            }
        });
        findViewById(R.id.dialog_add_location__Button_Cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });

    }


}
