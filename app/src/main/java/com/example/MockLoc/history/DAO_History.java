package com.example.MockLoc.history;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import com.example.MockLoc.CurrentApp;

import java.util.ArrayList;

/**
 * Author: Gordon
 * Date: 2014/6/23 14:18
 * Todo:
 */
public class DAO_History {

    public static void clearHistory(Context context) {
        DbHelper dbHelper = CurrentApp.obtainApp(context).dbHelper;
        dbHelper.execSQL("delete from history", null);
    }

    public static void addLocationHistory(Context context, Location location, String remark) {
        DbHelper dbHelper = CurrentApp.obtainApp(context).dbHelper;
        dbHelper.execSQL("INSERT into history (latitude, longitude, remark) VALUES( ?,?,?);", new Object[]{
                location.getLatitude(), location.getLongitude(), remark
        });
    }

    public static ArrayList<HistoryBean> getLocationHistories(Context context) {
        DbHelper dbHelper = CurrentApp.obtainApp(context).dbHelper;
        Cursor cursor = dbHelper.rawQuery("select * from history", null);
        ArrayList<HistoryBean> historyBeans = new ArrayList<HistoryBean>();
        while (cursor.moveToNext()) {
            HistoryBean history = new HistoryBean();
            history.id = cursor.getInt(0);
            history.latitude = cursor.getString(1);
            history.longitude = cursor.getString(2);
            history.remark = cursor.getString(3);
            historyBeans.add(history);
        }
        cursor.close();
        return historyBeans;

    }
}
