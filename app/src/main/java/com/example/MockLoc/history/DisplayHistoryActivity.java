package com.example.MockLoc.history;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.*;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.MockLoc.CurrentApp;
import com.example.MockLoc.R;

import java.util.ArrayList;

/**
 * Author: Gordon
 * Date: 2014/6/23 15:03
 * Todo:
 */
public class DisplayHistoryActivity extends ListActivity {

    CurrentApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = CurrentApp.obtainApp(this);
        app.addActivity(this);
        setTheme(app.currentThemeId);

        //  get histories

        setListAdapter(new HistoryAdapter(this,
                DAO_History.getLocationHistories(this)));

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HistoryBean history = (HistoryBean) parent.getAdapter().getItem(position);
                app.latitude = Double.valueOf(history.latitude);
                app.longitude = Double.valueOf(history.longitude);
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        app.removeActivity(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, R.id.menu_clear_history, 0, R.string.s_clear_history);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_clear_history:
                DAO_History.clearHistory(this);
                ((BaseAdapter) getListAdapter()).notifyDataSetChanged();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private static final class HistoryAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<HistoryBean> historyBeans;

        private HistoryAdapter(Context context, ArrayList<HistoryBean> historyBeans) {
            this.context = context;
            this.historyBeans = historyBeans;
        }

        @Override
        public int getCount() {
            return historyBeans.size();
        }

        @Override
        public Object getItem(int position) {
            return historyBeans.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.item_history, parent, false);
                holder = new ViewHolder();
                holder.tv_Latitude = (TextView) convertView.findViewById(R.id.item_history__TextView_Latitude);
                holder.tv_Longitude = (TextView) convertView.findViewById(R.id.item_history__TextView_Longitude);
                holder.tv_Remark = (TextView) convertView.findViewById(R.id.item_history__TextView_Remark);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            HistoryBean history = historyBeans.get(position);
            holder.tv_Remark.setText(history.remark);
            holder.tv_Longitude.setText(history.longitude);
            holder.tv_Latitude.setText(history.latitude);
            return convertView;
        }
    }

    private static final class ViewHolder {
        public TextView tv_Longitude, tv_Latitude, tv_Remark;
    }
}
