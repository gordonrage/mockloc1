package com.example.MockLoc;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Process;

/**
 * Author: Gordon
 * Date: 2014/6/11 16:40
 * This is a broadcast receiver which response for clear notification bar app icon that belong to its.
 */
public class NotificationBroadcast extends BroadcastReceiver {

    public static final String INTENT_ACTION_CLOSE = "com.example.MockLoc.action.NotificationBroadcast_Close";
    public static final String INTENT_ACTION_CLICK = "com.example.MockLoc.action.NotificationBroadcast_Click";


    @Override
    public void onReceive(Context context, Intent intent) {
        CurrentApp app = CurrentApp.obtainApp(context);
        if (INTENT_ACTION_CLICK.equals(intent.getAction())) {
        } else if (INTENT_ACTION_CLOSE.equals(intent.getAction())) {
            NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            nm.cancelAll();
            app.exit();
            context.stopService(new Intent(context, LocationRetainService.class));
            Process.killProcess(Process.myPid());
        }

    }
}
