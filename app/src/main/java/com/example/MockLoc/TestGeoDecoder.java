package com.example.MockLoc;

import android.location.Address;
import android.location.Geocoder;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import java.util.List;

/**
 * Author: Gordon
 * Date: 2014/6/19 11:42
 * Todo:
 */
public class TestGeoDecoder extends AndroidTestCase {


    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }


    @SmallTest
    public void testDecode() throws Exception {
        Geocoder geocoder = new Geocoder(getContext());
        List<Address> addresses = geocoder.getFromLocationName("中国湖南长沙市岳麓区商学院", 2);
        for (Address address : addresses) {
            GD.i(address);
        }
        assertNotNull(addresses);
        assertTrue(addresses.size() == 0);
    }
}
