package com.example.MockLoc;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.*;
import android.preference.PreferenceManager;
import com.example.MockLoc.setting.SettingsActivity;

/**
 * Author: Gordon
 * Date: 2014/6/18 16:41
 * Todo:
 */
public class LocationRetainService extends Service {


    LocationManager locationManager;
    boolean startMock;
    private CurrentApp app;

    Handler handler;

    SharedPreferences sharedPreferences;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = CurrentApp.obtainApp(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.addTestProvider(LocationManager.GPS_PROVIDER, false, false, false, false, true, false, false, 0, 5);
        locationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER, true);
        locationManager.setTestProviderStatus(LocationManager.GPS_PROVIDER, 2, null, System.currentTimeMillis());
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startMock = true;
        new LocationUpdateTask().start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        startMock = false;
    }

    private void updateMockLocation() {

        final Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setLatitude(app.latitude);
        location.setLongitude(app.longitude);
        location.setAccuracy(500f);
        location.setTime(System.currentTimeMillis());
        location.setBearing(0.5f);
        location.setProvider(LocationManager.GPS_PROVIDER);
        location.setSpeed(0.1f);
        StatisticTool.trackLocationUsed(app, location);

        if (Build.VERSION.SDK_INT >= 17) {
            location.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.setTestProviderLocation(LocationManager.GPS_PROVIDER, location);
        if (sharedPreferences.getBoolean(SettingsActivity.KEY_DEBUG, false)) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    GD.toast(location, LocationRetainService.this);
                }
            });
        }
    }


    private class LocationUpdateTask extends Thread {
        @Override
        public void run() {
            super.run();
            try {
                while (startMock) {
                    updateMockLocation();
                    Thread.sleep(sharedPreferences.getInt(SettingsActivity.KEY_LOC_UPDATE_PERIOD, 2) * 1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
